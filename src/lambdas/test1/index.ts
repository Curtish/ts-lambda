import * as AWS from "aws-sdk";
const s3 = new AWS.S3();

export function handler(event: AWSLambda.S3CreateEvent, context: AWSLambda.Context): void {
  console.log('Received event: ', JSON.stringify(event, null, 2));
  
  event.Records.forEach(record => {
    const getParams: AWS.S3.GetObjectRequest = {
      Bucket: record.s3.bucket.name,
      Key:    record.s3.object.key
    };

    s3.getObject(getParams, (error: AWS.AWSError, data: AWS.S3.GetObjectOutput) => {
      if(error) console.log("getObject error: ", error);
      else {
        const parsed: AWS.S3.Body = {
          date:  new Date().toUTCString(),
          event: data.Body.toString()
        };

        const putParams: AWS.S3.PutObjectRequest = {
          Bucket: "lambda-test-bucket-620367696693",
          Key:    "parsed",
          Body:   JSON.stringify(parsed)
        };

        s3.putObject(putParams, (error: AWS.AWSError, data: AWS.S3.PutObjectOutput) => {
          if(error) console.log("Put object error: ", error);
          else      console.log(data);
        });
      }
    });
  });
};
